function f(s) 
    --[[Implementation of Python3 f-strings.

    print(f"this -> {x} <- is evaluated as code and returns the value!")
    ]]--
    while string.match(s, "%b{}") ~= nil do
        local ln = string.match(s, "%b{}")
        print(ln)
        s = string.gsub(s, ln, "EXT")
    end
    return s
end
