
function loadFilesystem(id)
    --[[
    TODO
    add checks for if game doesnt exist, and if loadFS doesnt exist

    ]]--
    filesystem_id = id
    --init_fs()
    --_G[id]["loadFS"]()
end

function makeFile(path, body)
    if love.filesystem.getInfo( path ) == nil then
        love.filesystem.write(path, body)
    end
end

function makeDir(path)
    if love.filesystem.getInfo( path ) == nil then
        love.filesystem.createDirectory(path)
    end
end

filesys = {}
function filesys.createFile(path, body)
    makeFile(filesystem_id..path, body)
end

function filesys.createDir(path)
    makeDir(filesystem_id..path)
end

function init_fs()
    addLog("Loading filesystem...")

    if love.filesystem.getInfo( "universe.json" ) == nil then
        --file = love.filesystem.newFile( "universe.json" )
        love.filesystem.write("universe.json", [[{
    "cwd": "/",
    "prompt": "",
    "mode": "terminal",
    "delays": [],
    "autoplay_bgm": false,
    "autorun": ""
}]])
    end

    
    --[[
        TODO recreate the help docs

    ]]--
    makeDir(filesystem_id.."/bin")
    makeFile(filesystem_id.."/bin/audio.system", [[{"type": "executable", "signal": "extension"}]])
    makeFile(filesystem_id.."/bin/test.system", [[{"type": "executable", "signal": "extension"}]])
    makeDir(filesystem_id.."/sys")


    makeDir(filesystem_id.."/snd")
    local bgms = love.filesystem.getDirectoryItems( "audio/bgm/" )
    local sfxs = love.filesystem.getDirectoryItems( "audio/sfx/" )

    for c = 1, #bgms do
        local fn = bgms[c] -- audio.mp3
        local id = fn:SplitStr("%.")[1] --audio
        makeFile(filesystem_id.."/snd/"..id..".snd", [[{"type": "bgm", "id": "]]..id..[["}]])
    end

    for c = 1, #sfxs do
        local fn = sfxs[c] -- audio.mp3
        local id = fn:SplitStr("%.")[1] --audio
        makeFile(filesystem_id.."/snd/"..id..".snd", [[{"type": "sfx", "id": "]]..id..[["}]])

    end

    --addLog("Filesystem restored from backup.")
end