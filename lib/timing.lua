delays = {}

function addDelay(t, fn)
    -- fn must be a function name in the global namespace
    -- t is the delay
    -- function triggers when ticks = ticks + delay
    if _G[fn] ~= nil then
        table.insert(delays, {name = fn, time = ticks+t})
        setConfig("delays", delays)
        print(inspect(delays))
    end
end

function test()
    print("Tested!")
end

function executeGlobal(fn, arg)
    if _G[fn] ~= nil then
        _G[fn](arg)
    end
end
