require "lib.klua"
require "lib.class"

local CS_FileRegister = {}
function CS_FileRegister.new() -- initializer function
    local self = deepcopy(CS_FileRegister)
    self.name = ""
    self.path = ""
    self.body = ""
    return self
end;

    --[[write = function(self, text) end;
    append = function(self, text) end;
    prepend = function(self, text) end;
    replace = function(self, text, newtext) end;
    read = function(self) end;
    save = function(self) end;]]--


CodeSpeak = {}
function CodeSpeak.new()  -- initializer function
    self = deepcopy(CodeSpeak)
    self.variables = {}
    self.variables["_lineNumber"] = -1
    self.variables["_parserMode"] = ""
    --print(self.variables[":ln"])
    self.lines = {}
    self.commands = {
        echo = {
            scope = {"*"},
            execute = function(engine, s) engine:echo(s) end
        },
        comp = {
            scope = {"compile"},
            execute = function(engine, s) engine:echo(s) end
        },
        addi = {},
        subi = {},
        divi = {},
        muli = {},
        mark = {},
        jump = {},
        test = {},
        doif = {
            usage = "doif op1 type op2, command",
            help = [[Does command if check passes. 
            The op values are variable pointers, file pointers, strings, or numbers.

            Example: doif x == 1, echo "X is 1!"]]
        },
        grab = {},
        drop = {},
        setv = {
            scope = {"runtime", "repl"},
            help = "Defines variable k to have value v.",
            usage = "setv k v",
            execute = function(engine, ln) engine:set(ln, "") end
        },
        defn = {
            scope = {"compile"},
            help = "Defines a variable as an empty string with name v.",
            usage = "defn v",
            execute = function(engine, ln) engine:set(ln, "") end
        },
        rem = {
            scope = {"*"},
            execute = function(engine, ln) end;
        }
    }
    self.commands["-"] = self.commands["rem"]
    self.commands["?"] = self.commands["if"]
    self.labelRegister = {}
    self.str_re = [=[["`](.)+["`]]=]
    return self
end

function CodeSpeak.setMode(self, newm) return self:set("_parserMode", newm) end;
function CodeSpeak.setLineNumber(self, ln) return self:set("_lineNumber", ln) end;
function CodeSpeak.mode(self) return self:get("_parserMode") end;
function CodeSpeak.lineNumber(self) return self:get("_lineNumber") end;

function CodeSpeak.parse(self, line) -- main entrypoint function to process text line
    local cmd = line:SplitStr(" ")[1]:lower()
    local arg = line:sub(#cmd+2, #line)

    if self.commands[cmd] ~= nil then
        if has_value(self.commands[cmd].scope, self:mode()) or has_value(self.commands[cmd].scope, "*") then
            self.commands[cmd].execute(self, arg)
        end
    end
end;

function CodeSpeak.set(self, key, var) self.variables[key] = var end;

function CodeSpeak.get(self, key) return self.variables[key] end;

function CodeSpeak.getType(self, ln) 
        -- if surrounded by quotes, string
        -- ELSE:
        --      if number, number
        --      ELSE:
        --           if text, variable pointer
        --           if text starting with #, file pointer
end;
function CodeSpeak.echo(self, ln)
        -- get contents of ln
        -- then send the contents to writeln
        self:writeln(ln)
end;
function CodeSpeak.writeln(self, ln)
    -- todo colours
    print("["..self:lineNumber().."] "..ln)
end;
    
    --[[addCommand = function(self, name, fn) end;
    loadText = function(self, txt) end;
    loadFile = function(self, path) end;
    compile = function(self) end;

    -- if file register (#name), get contents of file
    -- if variable (plain name), get variable
    -- if string (ends and starts with quote marks), just get plain string
    contentOf = function(self, id) end;

    -- new idea for file register, make them assign to a variable
    -- distinction of file handle with normal variables will be file handle starts with #
    createFileRegister = function(self, name, path) end;
    dropFileRegister = function(self, name) end;
}]]--

return CodeSpeak