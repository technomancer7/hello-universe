require "lib.codespeak"

local x = CodeSpeak.new()
x:parse("echo test me")
local y = CodeSpeak.new()
y:set("_lineNumber", 1)
y:parse("echo line 1")
x:parse("! back to negative")

x:set("_parserMode", "compile")
x:parse "echoc this only runs in compile-time"

x:set("_parserMode", "runtime")
x:parse "echoc this only runs in compile-time, so wont run now"

--[[
require "lib.klua"
local test = {}
function test.new() -- initializer function
    local f = deepcopy(test)
    f.name = ""
    f.path = ""
    f.body = ""
    return f
end;
function test.setName(t, text) 
    t.name = text
    return t:getName()
end;
function test.getName(t) return t.name end;

local x = test.new()
local y = test.new()
local f = x:setName "test"
y:setName "lol"
print(f)
print(x:getName(), y:getName())
]]--