function love.conf(t)
    t.window.title = "Universal Terminal" 
    t.window.width = 800
    t.window.height = 600
    t.identity = "universalterminal"
end 
