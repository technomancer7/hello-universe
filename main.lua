json = require "lib.json"
inspect = require "lib.inspect"
klua = require 'lib.klua'
class = require "lib.clasp"

function initScripts()
    local files = love.filesystem.getDirectoryItems( "scripts/" )
    for c = 1, #files do
        local fn = files[c]
        if fn:hasSuffix(".lua") then
            local fx = "scripts."..fn:SplitStr("%.")[1]
            require(fx)
        end
    end
end

require "flow-control"
require "lib.timing"
require "lib.fs"
require "lib.universe"


--[[


ted command
same as cat functionally but with editing
has an x/y cursor variable that tracks where the pointer is, entering text inserts in to line[x] at string[y]

playlist
command to add song ID's to a playlist to randomly choose to play on launch
command to enable/disable auto-play on launch

shortcut system
binding terminal commands (or lua commands for advanced usage) to F-keys
if terminal, can just be read and sent to parser, for lua command the function must be in global

scout system
switches view to a radar view with the ship in center and scout controlled
u/d/l/r arrow keys control an `x/y velocity`, while held, velocity raises, else velocity lowers
velocity changes the scout drones X/Y coords

credits.txt
add a file somewhere for credits and music attributions

]]--

cmd = {}
history = {}
cmd_log = {}
log = {}
ticks = 0

cat = {filename = "", body = "", offset = 0, lineStart = 1, lineLength = 120}
filesystem_id = "filesystem"

audio = {bgm = {}, sfx = {}, now_playing = nil}

history_offset = 1
fullscreenterm = false

autocomplete_base = {"treads", "clear"}
--
filesystem = {}
cwd = "/"
prompt = ""
gmode = "terminal"

lastScene = nil
lastCat = {}

function closeGame()
    _G[filesystem_id]["cleanup"]()
    filesystem_id = "filesystem"
    love.window.setTitle( "Universal Terminal" )
end

function openGame(id)
    -- TODO
    -- add checks
    -- if init, cleanup, and loadFS don't exist inthe object, dont load its FS

    require("games."..id)
    loadFilesystem(id)
    _G[id]["init"]()
    love.window.setTitle( _G[id]["name"] )
end

function openHelp(target)
    addLog("Opening doc/"..target..".help")
    if love.filesystem.getInfo( filesystem_id.."/doc/"..target..".help" ) ~= nil then
        openInCat(loadJson(filesystem_id.."/doc/"..target..".help"), cwd..target)
    else
        addLog(target.." not found.")
    end
end
--

function catScene()
    local lines = cat["body"]:SplitStr("\n")
    love.graphics.print(cat["filename"].." ("..cat["offset"].."/"..#lines..")", 10, 0)

    local catsp = 30

    for ln = cat["offset"]+1, #lines do
      love.graphics.print(tostring(ln).." "..lines[ln]:sub(cat["lineStart"], cat["lineLength"]+cat["lineStart"]), 0, catsp)
      catsp = catsp + 20
    end
end

function openInTreads(f, filepath) openInCat(f, filepath) end

function openInCat(f, filepath)
    --local f =
    if f["type"] == "text" then
        cat["filename"] = filepath
        cat["body"] = f["contents"]
        cat["lines"] = f["contents"]:SplitStr("\n")

        setHook("keypressed", "cat", function(key)
            if key == "down" then
                cat["offset"] = cat["offset"] + 1
            elseif key == "up" then
                cat["offset"] = cat["offset"] - 1
                if cat["offset"] < 0 then cat["offset"] = 0 end
            elseif key == "pagedown" then
                cat["offset"] = cat["offset"] + 10
            elseif key == "pageup" then
                cat["offset"] = cat["offset"] - 10
                if cat["offset"] < 0 then cat["offset"] = 0 end
            elseif key == "home" then
                cat["lineStart"] = 1
                cat["offset"] = 0
            elseif key == "right" then
                cat["lineStart"] = cat["lineStart"] + 1
            elseif key == "left" then
                cat["lineStart"] = cat["lineStart"] - 1
                if cat["lineStart"] < 1 then cat["lineStart"] = 1 end

            elseif key == "q" then
                revertScene("quit")

            elseif key == "m" then
                revertScene("min")
            end
        end)
        setHook("unhook", "cat", function(s)
            if s == nil then s = "quit" end
            print("Closing cat type "..s)
            unHook("keypressed", "cat")
        end)

        setScene("cat", catScene)
    else
        addLog("File "..filepath.." is not supported in this application.")
    end
end



--[[
File structure
All "files" in the game are real files in the save directory, which are essentially json files telling the game what to do when it's accessed


{
  "type": "", //type of file, example: text, executable
  "signal": "drone:1", //for executables, command string to send to the parser to figure out what the exe is trying to do
  "release": "drone:0", //signal to send when process "ends"
  "contents": "This is a text file!", //for text type files, just text contents to print
}

Format for exe signals is <name>.<args>.<args...>

So, drone.1 is a drone signal, with argument of 1

]]--



function processCmd(line)
    local parts = line:SplitStr(" ")
    local com = parts[1]
    table.remove(parts, 1)
    local arg = table.concat(parts, " ")

    if com == "dir" or com == "ls" then
        addLog("Contents of "..cwd )
        --files = love.filesystem.getDirectoryItems( love.filesystem.getSaveDirectory().."/filesystem"..cwd )
        local files = love.filesystem.getDirectoryItems( filesystem_id..cwd )
        for c = 1, #files do
            if love.filesystem.getInfo( filesystem_id..cwd..files[c] )["type"] == "directory" then
                addLog(files[c].."/")
            else
                addLog(files[c])
            end
            
        end
        return true
    elseif com == "bin" then
        addLog("Contents of /bin" )
        --files = love.filesystem.getDirectoryItems( love.filesystem.getSaveDirectory().."/filesystem"..cwd )
        local files = love.filesystem.getDirectoryItems( filesystem_id.."/bin" )
        for c = 1, #files do
            if files[c]:hasSuffix(".system") then
                addLog(files[c]:SplitStr("%.")[1])
            end
        end
        return true
    elseif com == "cd" then
        --[[if arg:contains("../") then
            addLog("Illegal operation.")
            return
        end]]--
        if arg == "/" or arg == "" then
            cwd = "/"
        elseif arg:hasPrefix("/") then
            tmp = filesystem_id..arg
            if love.filesystem.getInfo(tmp) ~= nil then
                cwd = arg.."/"
                addLog(cwd)
            end

        else
            tmp = filesystem_id..cwd..arg
            if love.filesystem.getInfo(tmp) ~= nil then
                cwd = cwd .. arg.."/"
                addLog(cwd)
            end
        end
        return true

    elseif com == "load" then
        openGame(arg)
    elseif com == "quit" then
        closeGame()
        --restoreFromBackup()
    elseif com == "cp" then
    elseif com == "mv" then
    elseif com == "help" then
        if arg == "" then
            local files = love.filesystem.getDirectoryItems( filesystem_id.."/doc/" )
            for c = 1, #files do
                 local fn = files[c]
                 if fn:hasSuffix(".help") then
                     addLog(fn:SplitStr("%.")[1])
                 end
            end
            return
        end
        openHelp(arg)

    elseif com == "tedium" or com == "ted" then
        if arg == "" then return end
        addLog("Opening "..cwd..arg)
        if love.filesystem.getInfo( filesystem_id..cwd..arg ) ~= nil then
            --openInTed(loadJson(filesystem_id.."/"..cwd..arg), cwd..arg)
        else
            addLog(cwd..arg.." not found.")
        end
    elseif com == "treads" or com == "trd" then
        if arg == "" then return end
        addLog("Opening "..cwd..arg)
        if love.filesystem.getInfo( filesystem_id..cwd..arg ) ~= nil then
            openInCat(loadJson(filesystem_id.."/"..cwd..arg), cwd..arg)
        else
            addLog(cwd..arg.." not found.")
        end
    elseif com == "install" then
        -- installs from .pkg files
        -- these files have keys of files and a destination directory
    elseif com == "rm" or com == "del" then
        if arg == "" then return end
        --[[if arg:contains("../") then
            addLog("Illegal operation.")
            return
        end]]--
        print("filesystem"..cwd..arg)
        if love.filesystem.getInfo( filesystem_id..cwd..arg ) ~= nil then
            love.filesystem.remove(filesystem_id..cwd..arg)
        end
    elseif com == "wait" then
        addDelay(100, "test")

    elseif com == "echo" then
        if arg == "" then return end
        addLog(arg)

    elseif com == "cls" or com == "clear" then
        history = {}

    elseif com == "kill" then
        if arg == "" then return end
        local files = love.filesystem.getDirectoryItems( filesystem_id.."/bin/" )
        for c = 1, #files do
             local fn = files[c]
             print(fn)
             if fn == arg..".system" then
                 local f = loadJson(filesystem_id.."/bin/"..fn)
                 if f["type"] == "executable" then
                    if f["signal"] == "extension" then
                        addLog("System file "..fn.." is not an daemonic process.")
                    else
                        if f["release"] == nil then
                            addLog("System daemon "..fn.." does not define a release signal.")
                        else
                            addLog("RELEASE "..f["release"])
                            handleSignals(f["release"]:SplitStr(":")[1], f["release"]:SplitStr(":")[2])
                            return true
                        end
                    end
                 end
             end
        end
    else
       local files = love.filesystem.getDirectoryItems( filesystem_id.."/bin/" )
       for c = 1, #files do
            local fn = files[c]
            if fn == com..".system" then
                local f = loadJson(filesystem_id.."/bin/"..fn)
                if f["type"] == "executable" then
                    if f["signal"] == "extension" then
                        if _G["sh_"..com] ~= nil then
                            _G["sh_"..com](arg)
                            return true
                        end
                    else
                        addLog("SIG "..f["signal"])
                        handleSignals(f["signal"]:SplitStr(":")[1], f["signal"]:SplitStr(":")[2])
                        return true
                    end

                end
            end
       end
       return false
    end
end

function handleSignals(signame, sigln)
    execHook("signal", signame, sigln)
    --TODO move to a signal signame hook
    --[[addLog("sigint "..signame.." "..sigln)
    execHook("signal", "signame", sigln)
    if signame == "drone" then
        if sigln ~= "0" then
            addLog("Activating Drone subsystem "..sigln)
            setDrone(sigln)
            loadDrones()
        else
            drones = {}
            setDrone("")
        end
    end]]--
end


function sendCmd()
    y = table.concat(cmd, "")
    table.insert(history, 1, "> "..y)
    table.insert(cmd_log, 1, y)
    cmd = {}

    if processCmd(y) == false then
        playSfx("error")
    end
end

function playBgm(id)
    if audio["now_playing"] ~= nil then
        love.audio.stop(audio["bgm"][audio["now_playing"]])
    end
    love.audio.play(audio["bgm"][id])
    audio["now_playing"] = id
    addLog("Now playing "..id)
end
function stopBgm()
    if audio["now_playing"] ~= nil then
        love.audio.stop(audio["bgm"][audio["now_playing"]])
    end
    audio["now_playing"] = nil
end

function playSfx(id)
    love.audio.stop(audio["sfx"][id])
    love.audio.play(audio["sfx"][id])
end

function love.load()
    -- SYSTEM --
    init_fs()
    cwd = getConfig("cwd")
    prompt = getConfig("prompt")
    gmode = getConfig("mode")


    delays = getConfig("delays")

    -- AUDIO --
    -- audio[] = []
    local bgms = love.filesystem.getDirectoryItems( "audio/bgm/" )
    for c = 1, #bgms do
        local fn = bgms[c] -- audio.mp3
        local id = fn:SplitStr("%.")[1] --audio
        audio["bgm"][id] = love.audio.newSource("audio/bgm/"..fn, "stream")
    end
    local sfxs = love.filesystem.getDirectoryItems( "audio/sfx/" )
    for c = 1, #sfxs do
        local fn = sfxs[c] -- audio.mp3
        local id = fn:SplitStr("%.")[1] --audio
        audio["sfx"][id] = love.audio.newSource("audio/sfx/"..fn, "static")
    end
    print(inspect(audio))

    -- GRAPHICS --
    font = love.graphics.newFont( "graphics/FiraCode-Bold.ttf" )
    love.graphics.setFont( font )

    -- Load scripts --
    initScripts()
end

function setCwd(s)
    cwd = s
    setConfig("cwd", s)
end

function setPrompt(s)
    prompt = s
    setConfig("prompt", s)
end

function setMode(s)
    gmode = s
    setConfig("mode", s)
end

function addLog(s) table.insert(history, 1, s) end

function love.update(dt)
    ticks = ticks + 1
    if ticks == 10 then addLog("Loading Universal Terminal v50.1.5...") end

    if #delays > 0 then
        local dl = shallowcopy(delays)
        for c = 1, #dl do
            if dl[c]["time"] == ticks then
                print("Execute "..dl[c]["name"])
                executeGlobal("test")
                table.remove(delays, c)
                setConfig("delays", delays)
                return
            end
        end
    end

    execHooks("tick", ticks)
end


function drawTerminalWindowFullscreen()
    startp = 560
    for c = history_offset, #history do
         love.graphics.print("  "..history[c] , 0, startp, 0, 1)
         startp = startp - 20
    end

    if ticks < 150 then 
        love.graphics.print("Loading terminal input system...", 0, 580, 0, 1)
    else
        y = table.concat(cmd, "")
        love.graphics.print(prompt.." > "..y.."_", 0, 580, 0, 1)
    end

    if getMouseBounds(0, 30, 0, 20) then
        love.graphics.print({{255, 255, 255, 1}, "< F2"}, 0, 0, 0, 1)
    else
        love.graphics.print({{255, 255, 255, 0.5}, "< F2"}, 0, 0, 0, 1)
    end
    
end

function getMouseBounds(xs, xe, ys, ye)
    if love.mouse.getX() > xs and love.mouse.getX() < xe and love.mouse.getY() > ys and love.mouse.getY() < ye then
        return true
    else
        return false
    end
end

function love.mousepressed( x, y, button, istouch, presses )
    if gmode == "terminal" then
        if getMouseBounds(0, 111, 420, 435) and not fullscreenterm then
            fullscreenterm = true
        end
        if getMouseBounds(0, 30, 0, 20) and fullscreenterm then
            fullscreenterm = false
        end
    end
end

function drawTerminalWindow()
    love.graphics.print("------------------------------------------------------------------------------------------------------------------", 
                        0, 430, 0, 1)

    
    if getMouseBounds(0, 111, 420, 435) then
        love.graphics.print({{255, 255, 255, 1}, "(F2: Fullscreen)"}, 1, 420, 0, 1)
    else
        love.graphics.print({{255, 255, 255, 0.5}, "(F2: Fullscreen)"}, 1, 420, 0, 1)
    end
    
    if history[1] ~= nil then love.graphics.print({{200, 200, 200, 0.8}, "  "..history[1] }, 0, 560, 0, 1) end
    if history[2] ~= nil then love.graphics.print({{200, 200, 200, 0.7}, "  "..history[2] }, 0, 540, 0, 1) end
    if history[3] ~= nil then love.graphics.print({{200, 200, 200, 0.5}, "  "..history[3] }, 0, 520, 0, 1) end
    if history[4] ~= nil then love.graphics.print({{200, 200, 200, 0.4}, "  "..history[4] }, 0, 500, 0, 1) end
    if history[5] ~= nil then love.graphics.print({{200, 200, 200, 0.3}, "  "..history[5] }, 0, 480, 0, 1) end
    if history[6] ~= nil then love.graphics.print({{200, 200, 200, 0.2}, "  "..history[6] }, 0, 460, 0, 1) end
    if history[7] ~= nil then love.graphics.print({{200, 200, 200, 0.1}, "  "..history[7] }, 0, 440, 0, 1) end

    if ticks < 150 then 
        love.graphics.print("Loading terminal input system...", 0, 580, 0, 1) 
    else
        y = table.concat(cmd, "")
        love.graphics.print(prompt.." > "..y.."_", 0, 580, 0, 1) 
    end

    

    if ticks > 70 then
        --if drone == "" then
        love.graphics.print({{150, 0, 0, 0.5}, "-- VIEWPORT 2 --"}, 600, 200, 0, 1)
        --TODO move this to a hook in game
        --[[else
            
            local fstrt = 200
            for k, v in pairs(drones) do
                --print(k.." "..inspect(v))
                love.graphics.print({{0, 155, 0, 0.5}, k}, 750, fstrt, 0, 1)
                love.graphics.print({{0, 155, 0, 0.5}, v["health"].."/100"}, 750, fstrt+20, 0, 1)
                love.graphics.print({{0, 155, 0, 0.5}, v["energy"].."/100"}, 750, fstrt+40, 0, 1)

                fstrt = fstrt + 80
            end
        end]]--
        
        --if nav == "" then
        love.graphics.print({{150, 0, 0, 0.5}, "-- VIEWPORT 1 --"}, 0, 200, 0, 1)
        --else
        --end   
    end
    
end

function love.draw()
    if gmode == "terminal" then 
        if fullscreenterm then
            drawTerminalWindowFullscreen()
        else
            drawTerminalWindow()
        end
    elseif gmode == "scene" and scene ~= nil then
        scene["fn"]()
    end
end

function love.keypressed(key)
    if ticks < 150 then return end

    if gmode == "terminal" then
        if key == "tab" then
            local y = table.concat(cmd, "")
            local parts = y:SplitStr(" ")
            --curp = parts[#parts]
            --print("Checking to complete....")
            --print(parts[#parts])
            for c = 1, #autocomplete_base do
                if autocomplete_base[c]:hasPrefix(parts[#parts]) then 
                    parts[#parts] = autocomplete_base[c]
                    local ps = table.concat(parts, " ")
                    --print("ps "..ps)
                    cmd = {}
                    for i in string.gmatch(ps, ".") do
                        print(i)
                        table.insert(cmd, i)
                    end
                end
            end  

            local files = love.filesystem.getDirectoryItems( "filesystem"..cwd )
            for c = 1, #files do
                 local fn = files[c]
                 if fn:hasPrefix(parts[#parts]) then 
                    parts[#parts] = fn
                    local ps = table.concat(parts, " ")
                    --print("ps "..ps)
                    
                    cmd = {}
                    for i in string.gmatch(ps, ".") do
                        print(i)
                        table.insert(cmd, i)
                    end
                    return
                end
            end

            local files = love.filesystem.getDirectoryItems( filesystem_id.."/bin/" )
            for c = 1, #files do
                local fn = files[c]
                if fn:hasPrefix(parts[#parts]) then 
                   parts[#parts] = fn
                   local ps = table.concat(parts, " ")
                   --print("ps "..ps)
                   ps = ps:SplitStr("%.")[1]
                   cmd = {}
                   for i in string.gmatch(ps, ".") do
                       print(i)
                       table.insert(cmd, i)
                   end
               end
            end
        end
        if fullscreenterm then
            if key == "home" then
                history_offset = 1
            end
        
            if key == "pageup" then
                history_offset = history_offset + 10
            end
        
            if key == "pagedown" then
                history_offset = history_offset - 10
                if history_offset <= 1 then history_offset = 1 end
            end
        end


        if key == "f2" then
            fullscreenterm = not fullscreenterm
        end

        if key == "backspace" then
            table.remove(cmd)
        end

        if key == "return" then
            sendCmd()
        end

        if has_value(alpha, key) then
            --[[ to re-enable when a better keypress sound is sound and the stuttering is fixed
            love.audio.stop(keypress)
            keypress:setPitch(math.random(80, 120)/100)
            love.audio.play(keypress)]]--
            if key == "space" then table.insert(cmd, " ")  else table.insert(cmd, key)  end
        end
    else
        if key == "f12" then
            revertScene()
        end
    end

    execHooks("keypressed", key)
end
