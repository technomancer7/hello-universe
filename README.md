# Hello, Universe!

A simple terminal emulator roguelike.
(Currently in pre-pre-pre alpha, basically just concept at this point)

Current plan is, a simple randomly generated adventure, where your scavenger ship, armed with
various drones and utilities, travels from derelict to derelict, and occasionally planets and events in-between, where you can send out drones to explore, salvage, and bring back resources to survive, all through a simple terminal interface, with a subtle but (imo) interesting story being unravelled along the way.

The gameplay of the terminal is planned to have quite a lot of depth, but I'm very inexperienced with Lua so we'll have to wait and see how that turns out, it'll be a learning experience I'm sure.


## Story idea

It is an undisclosed year, but far in to the future. Humanity has advanced, multi-planatery, but during it's expansion, it angered an unknown enemy, which led to the dispersal of the human explorers.
Many died, the rest escaped, and went in to hiding. 
And now, humanity's previously occupied space is full of the wreckage and remnants of the war.

You are a survivor, left adrift in space, and you must confront the ghosts of history to survive, salvaging from the remains.
You will be able to utilize the Athena OS system to accomplish this, one of the weapons that was developed to combat the enemy. It is a highly advanced AI and operating system that will assist you in your journey.

## Mechanics
Main input method will be the terminal commands. Some basic mouse control may also be enabled.
Most operations are done through commands, such as navigating between wrecks and events in space, managing resources for defence and offense, and communicating with the OS, and maybe other parties.

The flow of the adventure is, you have options of where to go next, randomly generated, and you can choose any.
If the destination is a wreck, you dock, and send your drones in, controlled manually through the terminal to explore the wreck, room by room, gathering resources, etc.
Destinations may also be friendlies, events, human stations.
The more powerful your ship becomes, it may attract the attention of the unknown enemy, and the objective is essentially, survive, and either escape the area, or defeat the enemy.

## Meta
The game is also intended to be highly moddable. The core itself is Lua, which can be edited ofcourse, but also every function and system is designed to be modular.
Every "application" or "command" in the game is essentially implemented as a mod, to really make sure that the modding capabilities are advanced enough.
Implementing commands is a simple process, of dropping a JSON file in the filesystem directory that the game parses from, which has a key telling it what global Lua function to call. New Lua functions can then be written, and dropped in to the scripts directory, which is loaded at launch.
It's also trivial to implement new "scenes". A Scene in this context is, essentially, the entire current display in the window. By default, the terminal screen. But, in a simple extension file, you can write a new Scene that is an entire OS graphical display, or a minigame with its own controls and graphics, through the simple entry point of setScene(fn), where fn is a function that draws the game window using the LOVE graphics API. Again, this is how the default window setup is implemented. And to return to the default window, revertScene() takes you back to the terminal, which can be called arbitrarily within the extension.
Another useful tool for extensions is the Hooks system (WIP), which makes it easy to add new functionality to the core system. For example, in the core keypressed function which handles typing in to the terminal, it checks for keypressed hooks, and runs those, passing through the key pressed to that arbitrary function.
This provides a simple entry point, where a custom scene can easily write a hook function that takes keypresses exactly the same way that the core does. The idea eventually is these hooks will be implemented at every level of the core, allowing any aspect to be extended easily.
