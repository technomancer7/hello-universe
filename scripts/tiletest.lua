tile_entities = {}

TileEntity = class {
    init = function(self, s, x, y) -- initializer function
      self.s = s
      self.x = x
      self.y = y
      self.name = ""
    end;

 

    tick = function(self)
    end;

    draw = function(self)
        love.graphics.print(self.s, self.x, self.y, 0, 1)
        if self.name ~= "" then love.graphics.print("Player", self.x-7, self.y-10) end
    end;
}

Player = TileEntity:extend {
    init = function(self, s, x, y)
        TileEntity.init(self, s, x, y)
        self.name = "Player"
    end;

    tick = function(self)
        TileEntity.tick(self)
    end;
}

function tile_entities.spawn(entity, s, x, y)
    table.insert(tile_entities, entity(s, x, y))
end

function tileModeHook()
    love.graphics.print("Exit (F12)", 0, 0, 0, 1)
    for c = 1, #tile_entities do
        local ent = tile_entities[c]
        ent:draw()
    end
end

function sh_tiletest(arg)
    print("Switching to tile mode scene.")
    --love.keyboard.setKeyRepeat( true )
    -- Creating the Player object
    --spawn(Player, "#", 130, 130)

    -- Defining our keypress hook, for adding controls to the scene
    --setHook("keypressed", "space", function(key)

    --end)

    -- Defining our tick function, happens every frame, to update shooter_entities
    setHook("tick", "tile", function(t)
        for c = 1, #tile_entities do
            local ent = tile_entities[c]
            ent:tick()
        end

    end)

    -- Cleaning up when we end the scene
    setHook("unhook", "tile", function()
        print("Ending tile scene.")
        unHook("tick", "tile")
        --unHook("keypressed", "space")
        --love.keyboard.setKeyRepeat( false )
    end)

    -- Once setup is done,switch over to the scene
    setScene("tile", tileModeHook)
end