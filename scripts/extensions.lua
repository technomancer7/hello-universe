

function sh_audio(arg)
    local ac = arg:SplitStr(" ")
    if ac[1] == "stop" then
        stopBgm()
        addLog("Audio stopped.")
    elseif ac[1] == "list" then
        local files = love.filesystem.getDirectoryItems( filesystem_id.."/snd/" )
        for c = 1, #files do
            local fn = files[c]
            if audio["bgm"][fn:SplitStr("%.")[1]] ~= nil then 
                if fn:hasSuffix(".snd") then addLog(fn:SplitStr("%.")[1]) end 
            end   
        end
    elseif ac[1] == "play" then
        if love.filesystem.getInfo(filesystem_id.."/snd/"..ac[2]..".snd") ~= nil then
            local f = loadJson(filesystem_id.."/snd/"..ac[2]..".snd")
            if f["type"] == "bgm" then
                --love.audio.play(audio["bgm"][f["id"]])
                playBgm(f["id"])
            end
        end
    end
end