shooter_entities = {}

ShooterEntity = class {
    init = function(self, s, x, y) -- initializer function
      self.s = s
      self.x = x
      self.y = y
      self.xv = 0
      self.yv = 0
      self.name = ""
      self.moveSpeed = 2
    end;

    processMovement = function(self)
        self.x = math.floor(self.x)
        self.y = math.floor(self.y)
        self.xv = math.floor(self.xv*100)/100
        self.yv = math.floor(self.yv*100)/100

        if self.xv > 0 then self.x = self.x + self.xv end
        if self.xv < 0 then self.x = self.x - -1*self.xv end

        if self.yv > 0 then self.y = self.y + self.yv end
        if self.yv < 0 then self.y = self.y - -1*self.yv end

        if self.xv > 0.0 then self.xv = self.xv - 0.5 end
        if self.yv > 0.0 then self.yv = self.yv - 0.5 end

        if self.xv < 0.0 then self.xv = self.xv + 0.5 end
        if self.yv < 0.0 then self.yv = self.yv + 0.5 end

    end;

    tick = function(self)

        --self:processMovement()

        print(self.s, self.x, self.y, self.xv, self.yv)
    end;

    draw = function(self)
        love.graphics.print(self.s, self.x, self.y, 0, 1)
        if self.name ~= "" then love.graphics.print("Player", self.x-7, self.y-10) end
    end;
}

ShooterPlayer = ShooterEntity:extend {
    init = function(self, s, x, y)
        ShooterEntity.init(self, s, x, y)
        self.name = "Player"
    end;

    tick = function(self)
        ShooterEntity.tick(self)
    end;
}

function shooter_entities.spawn(entity, s, x, y)
    table.insert(shooter_entities, entity(s, x, y))
end

function spaceScout()
    love.graphics.print("Exit (F12)", 0, 0, 0, 1)
    for c = 1, #shooter_entities do
        local ent = shooter_entities[c]
        ent:draw()
    end
end

function sh_shootertest(arg)
    print("Switching to space scene.")
    --love.keyboard.setKeyRepeat( true )
    -- Creating the Player object
    shooter_entities.spawn(ShooterPlayer, "#", 130, 130)

    -- Defining our keypress hook, for adding controls to the scene
    --setHook("keypressed", "space", function(key)

    --end)

    -- Defining our tick function, happens every frame, to update shooter_entities
    setHook("tick", "space", function(t)
        for c = 1, #shooter_entities do
            local ent = shooter_entities[c]
            ent:tick()
        end

        if love.keyboard.isDown( "d" ) then --and shooter_entities[1].xv <= 2  then
            --shooter_entities[1].xv = shooter_entities[1].xv + shooter_entities[1].moveSpeed
            shooter_entities[1].x = shooter_entities[1].x + shooter_entities[1].moveSpeed
        end
        if love.keyboard.isDown( "a" ) then --and shooter_entities[1].xv > -2 then
            --shooter_entities[1].xv = shooter_entities[1].xv - shooter_entities[1].moveSpeed
            shooter_entities[1].x = shooter_entities[1].x - shooter_entities[1].moveSpeed
        end
        if love.keyboard.isDown( "s" ) then --and shooter_entities[1].yv <= 2 then
            --shooter_entities[1].yv = shooter_entities[1].yv + shooter_entities[1].moveSpeed
            shooter_entities[1].y = shooter_entities[1].y + shooter_entities[1].moveSpeed
        end
        if love.keyboard.isDown( "w" ) then --and shooter_entities[1].yv > -2 then
            --shooter_entities[1].yv = shooter_entities[1].yv - shooter_entities[1].moveSpeed
            shooter_entities[1].y = shooter_entities[1].y - shooter_entities[1].moveSpeed
        end
    end)

    -- Cleaning up when we end the scene
    setHook("unhook", "space", function()
        print("Ending space scene.")
        unHook("tick", "space")
        --unHook("keypressed", "space")
        --love.keyboard.setKeyRepeat( false )
    end)

    -- Once setup is done,switch over to the scene
    setScene("space", spaceScout)
end