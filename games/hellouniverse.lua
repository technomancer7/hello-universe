hellouniverse = {}


hellouniverse.defaultReadmeMessage = [[ -- [NOTE: This file is partially corrupt. This is a restored version from memory, there may be errors in format.] --\n\n\nHello there.\nThis may be surprising.\nThis message is meant for the survivor of [REDACTED]\nIf you read this message, get out of there. The [REDACTED] are coming.\nThey already destroyed the entire fleet.\nYour ship should have a basic drone and nav system, as well as a scout driver. That'll be enough to get out of the area.\nThere'll be enough salvages nearby, wrecks.\nScavenge those, find some resources.\nFrom there, just keep going.\nThe bastard [REDACTED] are vicious.\nIf they find you, they will end you.\nIf you ever feel that electric horrifying energy in the air...\nRun.\nMaybe it'll be too late.\nBut it's better to at least try,\nrather than just sit back and wait,\for whatever fate they have for you.\nHello there.\nThis may be surprising.\nThis message is meant for the survivor of [REDACTED]\nIf you read this message, get out of there. The [REDACTED] are coming.\nThey already destroyed the entire fleet.\nYour ship should have a basic drone and nav system, as well as a scout driver. That'll be enough to get out of the area.\nThere'll be enough salvages nearby, wrecks.\nScavenge those, find some resources.\nFrom there, just keep going.\nThe bastard [REDACTED] are vicious.\nIf they find you, they will end you.\nIf you ever feel that electric horrifying energy in the air...\nRun.\nMaybe it'll be too late.\nBut it's better to at least try,\nrather than just sit back and wait,\for whatever fate they have for you.\nHello there.\nThis may be surprising.\nThis message is meant for the survivor of [REDACTED]\nIf you read this message, get out of there. The [REDACTED] are coming.\nThey already destroyed the entire fleet.\nYour ship should have a basic drone and nav system, as well as a scout driver. That'll be enough to get out of the area.\nThere'll be enough salvages nearby, wrecks.\nScavenge those, find some resources.\nFrom there, just keep going.\nThe bastard [REDACTED] are vicious.\nIf they find you, they will end you.\nIf you ever feel that electric horrifying energy in the air...\nRun.\nMaybe it'll be too late.\nBut it's better to at least try,\nrather than just sit back and wait,\for whatever fate they have for you.\nHello there.\nThis may be surprising.\nThis message is meant for the survivor of [REDACTED]\nIf you read this message, get out of there. The [REDACTED] are coming.\nThey already destroyed the entire fleet.\nYour ship should have a basic drone and nav system, as well as a scout driver. That'll be enough to get out of the area.\nThere'll be enough salvages nearby, wrecks.\nScavenge those, find some resources.\nFrom there, just keep going.\nThe bastard [REDACTED] are vicious.\nIf they find you, they will end you.\nIf you ever feel that electric horrifying energy in the air...\nRun.\nMaybe it'll be too late.\nBut it's better to at least try,\nrather than just sit back and wait,\for whatever fate they have for you.\n[MESSAGE END]\n]]

hellouniverse.drones = {}
hellouniverse.name = "Hello, Universe!"
hellouniverse.drone = ""
hellouniverse.nav = ""

function hellouniverse.loadDrones()
    hellouniverse.drones = {}
    local files = love.filesystem.getDirectoryItems( filesystem_id.."/sys/" )
    for c = 1, #files do
         local fn = files[c]
         print(fn)
         if fn:hasSuffix(".drone") then
             local f = loadJson(filesystem_id.."/sys/"..fn)
             if f["type"] == "drone" then
                hellouniverse.drones[f["id"]] = f
             end
         end
    end
    print(inspect(hellouniverse.drones))
end

function hellouniverse.setDrone(s)
    hellouniverse.drone = s
    setConfig("drone", s)
end

function hellouniverse.init()
    if getConfig("progress") == nil then setConfig("progress", 0) end
    if getConfig("drone") == nil then setConfig("drone", "") end
    if getConfig("nav") == nil then setConfig("nav", "") end
    if getConfig("scout") == nil then setConfig("scout", "") end

    hellouniverse.drone = getConfig("drone")
    if hellouniverse.drone ~= "" then hellouniverse.loadDrones() end
    hellouniverse.nav = getConfig("nav")

    setHook("signal", "drone", function(sigln)   
        if sigln ~= "0" then
            addLog("Activating Drone subsystem "..sigln)
            hellouniverse.setDrone(sigln)
            hellouniverse.loadDrones()
        else
            hellouniverse.drones = {}
            hellouniverse.setDrone("")
        end
    end)
    -- make these a delay from init time
    --[[if ticks == 100 then
        if getConfig("progress") == 0 then
            playSfx("alert")
            table.insert(history, 1, "ERROR: Systems reporting problems that can not be resolved...")
            table.insert(history, 1, "ERROR: DRONE,NAV,SCOUT systems not connected.")
            table.insert(history, 1, "Refer to `help` command for activation.")
            --love.audio.play(bgm_drifting)
        else
            --love.audio.play(bgm_ambient)
        end
    end

    if ticks == 300 and getConfig("progress") == 0 then
        addLog("Filesystem has been corrupted.")
        addLog("Run `restore` to restore filesystem from backup.") 
        setConfig("progress", 1)
    end]]--
end

function hellouniverse.cleanup()
    unHook("signal", "drone")
end

-- deprecated
function hellouniverse.loadFS()
    addLog("Restoring filesystem...")
    makeDir(filesystem_id.."/bin")
    makeFile(filesystem_id.."/bin/drone.system", [[{"type": "executable", "signal": "drone:1", "release": "drone:0"}]])
    makeFile(filesystem_id.."/bin/scout.system", [[{"type": "executable", "signal": "scout:1", "release": "scout:0"}]])
    makeFile(filesystem_id.."/bin/nav.system", [[{"type": "executable", "signal": "nav:1", "release": "nav:0"}]])
    makeFile(filesystem_id.."/bin/audio.system", [[{"type": "executable", "signal": "extension"}]])
    makeFile(filesystem_id.."/bin/test.system", [[{"type": "executable", "signal": "extension"}]])

    makeFile(filesystem_id.."/readme.txt", [[
        {"type": "text", "contents": "]]..hellouniverse.defaultReadmeMessage..[[}"]])

    makeDir(filesystem_id.."/sys")
    makeFile(filesystem_id.."/sys/drone_basic.lib", [[{"type": "library", "key": "drone>1"}]])
    makeFile(filesystem_id.."/sys/albion.drone", [[{"type": "drone", "id": "albion", "health": 100, "energy": 100, "name": ""}]])

 

    addLog("Filesystem restored from backup.")
end