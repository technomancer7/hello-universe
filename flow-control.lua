scene = nil
hooks = {}

function setHook(name, id, fn)
    if hooks[name] == nil then hooks[name] = {} end
    hooks[name][id] = fn
end

function unHook(name, id)
   if hooks[name] == nil then return end
   hooks[name][id] = nil
end

function execHooks(name, args)
    if hooks[name] == nil then return nil end

    for _, value in pairs(hooks[name]) do
      value(args)
    end
end

function execHook(name, id, args)
    if hooks[name] == nil then return nil end
    if hooks[name][id] == nil then return nil end
    return hooks[name][id](args)
end

function setScene(id, fn)
    scene = {id = id, fn = fn}
    gmode = "scene"
end

function revertScene(s)
    execHook("unhook", scene["id"], s)
    scene = nil
    gmode = "terminal"
end
