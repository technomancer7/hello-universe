l = "../lib.inspect"
inspect = require(l)

hooks = {}
function setHook(name, id, fn)
    if hooks[name] == nil then hooks[name] = {} end

    hooks[name][id] = fn
    print(inspect(hooks))
end

function unHook(name, id)
   if hooks[name] == nil then return end

   hooks[name][id] = nil
end

function execHooks(name, args)
    if hooks[name] == nil then return nil end
    print(inspect(hooks[name]))

    for _, value in pairs(hooks[name]) do
      value(args)
    end
end

setHook("tick", "space", function(s) print(s) end)
execHooks("tick", "tiiii")
unHook("tick", "space")
print(#hooks["tick"])
print(inspect(hooks))
print(#hooks["tick"])